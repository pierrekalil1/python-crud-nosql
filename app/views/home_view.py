from flask import Flask, jsonify, request
from http import HTTPStatus
from app.exceptions.posts_exceptions import InvalidPostsError
from app.model.post import Post


def init_app(app: Flask):

    @app.get('/post')
    def get_posts():
        posts_list = Post.get_all()
        for post in posts_list:
            del post['_id']

        return jsonify(posts_list), HTTPStatus.OK


    @app.get('/post/<int:id>')
    def get_post_id(id):
        try:
            post_id = Post.get_post(id)
            return jsonify(post_id), HTTPStatus.OK
        except (InvalidPostsError, TypeError):
            return {"message": "Post não existe"}, HTTPStatus.BAD_REQUEST
        except KeyError as e:
            {"message": f"O {str(e)} não existe"}, HTTPStatus.BAD_REQUEST

    
    @app.post('/post')
    def create_post():
        data = request.json

        try:
            post = Post(**data)
            new_post = post.save()
            return {"message": "Dados cadastrados com sucesso"}, HTTPStatus.CREATED
        except (InvalidPostsError, TypeError):
            return {"message": "Dados invalidos"}, HTTPStatus.BAD_REQUEST
        except KeyError:
            {"msg": "Campo de cadastro não existe"}, HTTPStatus.BAD_REQUEST


    @app.patch('/post/<int:id>')
    def update_post(id):
        try:
            data = request.json
            Post.update_post(id, data)
            update = Post.get_post(id)
            # if update['_id'] not in update:
            #     return {"message": None}
            return jsonify(update), HTTPStatus.OK
        except InvalidPostsError:
            {"msg": "Dados não existem"}, HTTPStatus.BAD_REQUEST
        except KeyError as e:
            {"message": f"O {str(e)} selecionado não existe"}, HTTPStatus.BAD_REQUEST
        
    

    @app.delete('/post/<int:id>')
    def delete_post(id):
        try:
            delete = Post.delete_post(id)
            return delete, HTTPStatus.OK
        except InvalidPostsError:
            return {"msg": "Post não existe, busque por um post existente"}, HTTPStatus.BAD_REQUEST
        except KeyError as e:
            return {"msg": f"O {str(e)} selecionado não existe"}, HTTPStatus.BAD_REQUEST