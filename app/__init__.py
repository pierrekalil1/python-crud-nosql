from flask import Flask
from app.views import home_view


def create_app():
    app = Flask(__name__)

    app.config["JSON_SORT_KEYS"] = False

    home_view.init_app(app)

    return app