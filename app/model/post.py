import datetime
from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017/')
db = client['kenzie']

class Post():
    def __init__(self, title: str, author: str, tags, content: str ):
        data_atual = datetime.datetime.utcnow()
       
        self.create_at = data_atual.strftime(f'%d/%m/%Y %H:%M:%S')
        self.update_at = data_atual.strftime(f'%d/%m/%Y %H:%M:%S')
        self.title = title.title()
        self.author = author.title()
        self.tags = tags
        self.content = content.lower()
    

    @staticmethod
    def get_all():
        posts_list = list(db.posts.find())
        return posts_list
    

    @staticmethod
    def get_post(id):
        post = db.posts.find_one({"id": id})
        if post:
            del post['_id']
        return post


    def save(self):
        posts_list = list(db.posts.find())

        self.id = len(posts_list) + 1
        posts_list.append(self.__dict__)
        
        _id = db.posts.insert_one(self.__dict__).inserted_id
        new_post = db.posts.find_one({'_id': _id})

        return new_post
    

    @staticmethod
    def update_post(id_post, data):
        data_atual = datetime.datetime.utcnow()

        db.posts.update_one( {"id": id_post}, { "$set": {"title": data['title'], "author": data['author'], "tags": data['tags'], "content": data['content'], "update_at": data_atual.strftime(f'%d/%m/%Y %H:%M:%S')}})
        return {"message": "Dados do post atializados com sucesso"}


    @staticmethod
    def delete_post(id_post):
        posts_list = list(db.posts.find())

        for post in posts_list:
            del post['_id']
            if id_post == post['id']:
                db.posts.delete_one({"id": id_post})
                return {"message": "Post excluido com sucesso"}

            return {"message": "Post não existe"}
            
        